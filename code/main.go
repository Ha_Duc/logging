package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type LogEntry struct {
	Opstech   string      `json:"opstech,omitempty"`
	Operator  string      `json:"operator,omitempty"`
	Monitor   string      `json:"monitor,omitempty"`
	IndexName string      `json:"indexName,omitempty"`
	RespTime  int64       `json:"respTime,omitempty"`
	RespCode  string      `json:"respCode,omitempty"`
	RespBody  interface{} `json:"respBody,omitempty"`
}

type Body struct {
	Count    int  `json:"count"`
	Operator bool `json:"operator"`
}

var isFlag = true

func main() {

	routes := mux.NewRouter()
	routes.HandleFunc("/logging", loggingHandler).Methods("GET")
	routes.HandleFunc("/stop", stopLogging).Methods("GET")

	fmt.Println("Application is running on : 8082 .....")
	http.ListenAndServe(":8082", routes)
}

func loggingHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UFT-8")
	json.NewEncoder(w).Encode("Server is running")
	isFlag = true
	count := 0
	go func() {
		for isFlag {
			if count%2 == 0 {
				logEntry := LogEntry{
					Opstech:  "true",
					Operator: "true",
					RespCode: "OK",
					RespBody: Body{Count: count, Operator: true},
				}

				js, err := json.Marshal(logEntry)
				if err == nil {
					message := string(js)
					fmt.Println(message)

					// with logrus
					// log.SetFormatter(&log.JSONFormatter{})
					// log.WithFields(log.Fields{
					// 	"opstech":  "true",
					// 	"operator": "true",
					// 	"respCode": "OK",
					// 	"respBody": Body{Count: count, Operator: true},
					// }).Info()

				}
			} else {
				logEntry := LogEntry{
					Opstech:   "true",
					Monitor:   "true",
					RespCode:  "OK",
					IndexName: "log-monitor-serive-a",
					RespBody:  Body{Count: count, Operator: false},
				}

				js, err := json.Marshal(logEntry)
				if err == nil {
					message := string(js)
					fmt.Println(message)
				}
			}

			count++
			time.Sleep(time.Second * 5)
		}
	}()

}

func stopLogging(w http.ResponseWriter, r *http.Request) {
	isFlag = false
	w.Header().Set("Content-Type", "application/json; charset=UFT-8")
	json.NewEncoder(w).Encode("Server is stop")
}
